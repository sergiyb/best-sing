#!/bin/sh
# script for building BEst standalone applications
# assuming BEst code to ~/best folder

%short

mcc -o BEst -W main:BEst -T link:exe -d best_exec/for_testing -v ~/best/BEst.m -a ~/best/CODE


%long

%mcc -o BEst -W main:BEst -T link:exe -d ~/best/for_redistribution -v ~/best/BEst.m -a ~/best/BEst.m -a /home/users/sboroday/best/CODE -a ~/best/LAUNCH_ME_SCRIPT.m

% deploytool -package MCC BEst
% build an installer according to the project file


