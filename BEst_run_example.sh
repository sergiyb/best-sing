#!/bin/sh
# example script for execution of deployed BEst applications
#

MCR_ROOT=/usr/local/MATLAB/MATLAB_Runtime/v95 # depends on matlab vesrion, 95 for Matlab 2018

BEST_DIR=/usr/local/Multi_FunkIm_lab/BEst/application # depends on compiler args

$BEST_DIR/run_BEst.sh $MCR_ROOT MMEG.mat GMEG.mat vtcConn.mat sources cMEM "MEG" "1 1.9" "0 0.9"  normalization adaptive clusteringMethod static mspWindow 10 mspThresholdMethod arbitrary mspThreshold 0 neighborhoodOrder 4 spatialSmoothing 0.6 activeMeanInit 2 activeProbaInit 3 lambdaInit 1 activeProbaThreshold 0 activeVarCoef 0.05 inactiveVarCoef 0 noiseCovMethod 2 optimMethod fminunc useParallel 0
